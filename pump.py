# This is a sample Python script.
import datetime
import math

from sqlalchemy.orm import Session
from sqlalchemy import create_engine, select, func

from models import Tank3Log, Base
from sensors import HC_SR04
import RPi.GPIO as GPIO
import time
import configparser
import tomllib


if __name__ == '__main__':
    config = tomllib.load(open("config.toml", mode="rb"))
    config = config["pump"]
    PUMP_PIN = config["PUMP_PIN"]  # pump pin number in bcm pinout
    PUMP_SPEC = config["PUMP_SPEC"]  # pump spec [L/h]
    TANK3_MAXIMUM_LITRE = config["TANK3_MAXIMUM_LITRE"]  # unit: [L]
    TANK1_MAXIMUM_DISTANCE = config["TANK1_MAXIMUM_DISTANCE"]  # unit: [mm]　距離センサから水面までの距離がこの値以下の時ポンプを稼働させる
    TRIG_PIN = config["TRIG_PIN"]
    ECHO_PIN = config["ECHO_PIN"]

    PUMP_HOSE_DIAMETER = config["PUMP_HOSE_DIAMETER"]  # unit:[cm] ポンプに接続されるホースの直径
    PUMP_HOSE_LENGTH = config["PUMP_HOSE_LENGTH"]  # unit: [cm] ポンプに接続されるホースが^のようになってタンクに入るまでの長さ
    HOSE_LITRE = PUMP_HOSE_DIAMETER**2 * math.pi * PUMP_HOSE_LENGTH / (10**3)  # [cm^3] * [L/cm^3]
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PUMP_PIN, GPIO.OUT)

    try:
        parser = configparser.ConfigParser()
        parser.read_file(open("alembic.ini"))

        engine = create_engine(parser["alembic"]["sqlalchemy.url"])
        Base.metadata.create_all(engine)
        hc_sr04 = HC_SR04(TRIG_PIN, ECHO_PIN)
        GPIO.output(PUMP_PIN, GPIO.LOW)

        prev_time = time.monotonic_ns()

        while True:
            with Session(engine) as session:
                diff_time = time.monotonic_ns() - prev_time  # unit: [ns]
                # 前回ポンプを作動させていたらその時間分タンク3に水を送ったと仮定する
                if GPIO.input(PUMP_PIN) == GPIO.HIGH:
                    # [L/h]/[s/h]*[ns]*[s/ns] = [L/s]*[ns]*[s/ns] =[L]
                    dV = PUMP_SPEC / 3600 * diff_time * 10 ** -9
                    print(f"{datetime.datetime.now()}: dt={diff_time} dv={dV}ℓ")
                    log = Tank3Log(dV=dV, type="irrigation canal", detail="用水路からの取水による増加", is_approx=True)
                    session.add(log)
                    session.commit()

                tank3_liter = session.query(func.coalesce(func.sum(Tank3Log.dV), 0)).scalar()
                distance_mm = hc_sr04.read_distance()
                print(f"{datetime.datetime.now()}: distance: {distance_mm}mm tank3={tank3_liter}ℓ")
                # ポンプまで水がある時ポンプを稼働させる
                if distance_mm <= TANK1_MAXIMUM_DISTANCE and tank3_liter <= TANK3_MAXIMUM_LITRE:
                    GPIO.output(PUMP_PIN, GPIO.HIGH)
                else:
                    if GPIO.input(PUMP_PIN) == GPIO.HIGH:
                        dV = HOSE_LITRE
                        print(f"{datetime.datetime.now()}: dt={diff_time} dv={dV}ℓ")
                        log = Tank3Log(dV=dV, type="irrigation canal", detail="ポンプ停止にともなうホース内の水の自由落下", is_approx=True)
                        session.add(log)
                        session.commit()
                    GPIO.output(PUMP_PIN, GPIO.LOW)

                prev_time = time.monotonic_ns()
                time.sleep(1)
    except Exception as e:
        raise e
    finally:
        GPIO.output(PUMP_PIN, GPIO.LOW)
        GPIO.cleanup()

