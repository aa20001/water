# This is a sample Python script.
import datetime
import math
import signal
import tomllib

from sqlalchemy.orm import Session
from sqlalchemy import create_engine, select, func

import models
from models import Tank3Log, Base
from sensors import HC_SR04
import RPi.GPIO as GPIO
import time
import argparse
import psutil
import os
import configparser


PGM_ID = "watering"


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("target", help="どのくらい水をやるか", type=float)

    unit_group = parser.add_mutually_exclusive_group(required=True)
    unit_group.add_argument("-l", "--litre", help="{target}L水をやります", action="store_true")
    unit_group.add_argument("-s", "--seconds", help="{target}s水を出します", action="store_true")

    parser.add_argument("-f", "--force", help="ロックを解除して強制的に水をやります", action="store_true")
    parser.add_argument("-w", "--wait", help="ロックが解除されるまで待機します", action="store_true")

    args = parser.parse_args()

    config = tomllib.load(open("config.toml", mode="rb"))
    config = config["watering"]
    PUMP_PIN = config["PUMP_PIN"]  # pump pin number in bcm pinout
    PUMP_SPEC = config["PUMP_SPEC"]  # pump spec [L/h]
    PUMP_HOSE_DIAMETER = config["PUMP_HOSE_DIAMETER"]  # unit:[cm] ポンプに接続されるホースの直径
    PUMP_HOSE_LENGTH = config["PUMP_HOSE_LENGTH"]  # unit: [cm] ポンプに接続されるホースが^のようになってタンクに入るまでの長さ
    HOSE_LITRE = PUMP_HOSE_DIAMETER ** 2 * math.pi * PUMP_HOSE_LENGTH / (10 ** 3)  # [cm^3] * [L/cm^3] = [L]

    if args.litre:
        seconds = (args.target + HOSE_LITRE) / (PUMP_SPEC / 3600)  # [L] / [L/s] = [s]
        litre = args.target  # [L]
    else:
        seconds = args.target  # [s]
        litre = PUMP_SPEC / 3600 * args.target - HOSE_LITRE  # [L/s] * [s] = [L]

    try:
        parser = configparser.ConfigParser()
        parser.read_file(open("alembic.ini"))

        engine = create_engine(parser["alembic"]["sqlalchemy.url"])
        Base.metadata.create_all(engine)

        prev_time = time.monotonic_ns()

        with Session(engine) as session:
            # GPIOを使用できるプログラムは一つだけなのでロックを行う(楽観的)
            lock = session.get(models.Lock, {"name": PGM_ID})
            if lock and not lock.ended and psutil.pid_exists(lock.pid):
                if args.wait:
                    # もしロック中ならそれが終わるまで待つ
                    while lock and not lock.ended and psutil.pid_exists(lock.pid):
                        lock = session.get(models.Lock, {"name": PGM_ID})
                        time.sleep(0.5)
                elif args.force:
                    # もしロック中ならそのプログラムを強制終了する
                    os.kill(lock.pid, signal.SIGINT)
                else:
                    raise RuntimeError("locked")
            elif not lock:
                # もし初めてロックするならロック用の項目を生成する
                lock = models.Lock(name=PGM_ID, ended=False, pid=os.getpid())
                session.add(lock)
            lock.pid = os.getpid()
            lock.ended = False
            session.commit()

            try:
                GPIO.setmode(GPIO.BCM)
                GPIO.setup(PUMP_PIN, GPIO.OUT)

                start = time.monotonic()
                GPIO.output(PUMP_PIN, GPIO.HIGH)

                while start + seconds > time.monotonic():
                    pass
                GPIO.output(PUMP_PIN, GPIO.LOW)

                lock.ended = True
                session.commit()

                log = Tank3Log(dV=litre, type="watering", detail="水やり", is_approx=True)
                session.add(log)
                session.commit()
            finally:
                GPIO.output(PUMP_PIN, GPIO.LOW)

    except Exception as e:
        raise e
    finally:
        GPIO.cleanup()

