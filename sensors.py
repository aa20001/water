import RPi.GPIO as GPIO
import time


class HC_SR04:
    def __init__(self, trig_pin, echo_pin):
        self.trig_pin = trig_pin
        self.echo_pin = echo_pin

    def read_distance(self):
        """
        距離を計測し、mm単位で返す
        :return: distance_mm
        """
        # GPIOを初期化する
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.trig_pin, GPIO.OUT)
        GPIO.setup(self.echo_pin, GPIO.IN)

        # トリガーをかける
        GPIO.output(self.trig_pin, GPIO.HIGH)
        time.sleep(0.00001)  # 10μ秒待つ(データシートより)
        GPIO.output(self.trig_pin, GPIO.LOW)

        # echo_pinがHighになった時の時間を記録
        sig_off = time.monotonic_ns()
        while GPIO.input(self.echo_pin) == GPIO.LOW:
            sig_off = time.monotonic_ns()

        # echo_pinがLowになった時の時間を記録
        sig_on = time.monotonic_ns()
        while GPIO.input(self.echo_pin) == GPIO.HIGH and sig_on < sig_off+2e+8:
            sig_on = time.monotonic_ns()

        # 距離を計算する
        t = 20  # 音速の公式につかう気温を定義 単位はセルシウス温度[℃]
        c = (331.45 + 0.61 * t)  # 音速公式[m/s]
        duration = sig_on - sig_off  # echo_pinから取得したパルス幅を計算
        distance_mm = duration * c * 10**-6 / 2  # [m/s][ns]=[nm]=[m*10^9][10^-6]=[mm]
        return distance_mm


if __name__ == '__main__':
    try:
        inst = HC_SR04(4, 17)
        while True:
            print(inst.read_distance())
            time.sleep(1)
    except KeyboardInterrupt as e:
        GPIO.cleanup()
        raise e



