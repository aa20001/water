import argparse
import datetime

from sqlalchemy import String, DateTime, Boolean, create_engine, func
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, Session
from sqlalchemy.sql import functions
from sqlalchemy.types import Double
import configparser


class Base(DeclarativeBase):
    pass


class Lock(Base):
    __tablename__ = "lock"
    name: Mapped[str] = mapped_column(type_=String(length=128), nullable=False, primary_key=True)
    started_at: Mapped[datetime.datetime] = mapped_column(type_=DateTime, default=functions.now())
    ended: Mapped[bool] = mapped_column(type_=Boolean, default=False)
    pid: Mapped[int]


class Tank1Log(Base):
    __tablename__ = "tank1"

    id: Mapped[int] = mapped_column(primary_key=True)
    at: Mapped[datetime.datetime] = mapped_column("at", DateTime, default=functions.now())
    dV: Mapped[float] = mapped_column("dV", Double)  # unit is liter
    type: Mapped[str] = mapped_column("type", String(32))
    detail: Mapped[str] = mapped_column("detail", String(256))


class Tank2Log(Base):
    __tablename__ = "tank2"

    id: Mapped[int] = mapped_column(primary_key=True)
    at: Mapped[datetime.datetime] = mapped_column("at", DateTime, default=functions.now())
    dV: Mapped[float] = mapped_column("dV", Double)  # unit is liter
    type: Mapped[str] = mapped_column("type", String(32))
    detail: Mapped[str] = mapped_column("detail", String(256))


class Tank3Log(Base):
    __tablename__ = "tank3"

    id: Mapped[int] = mapped_column(primary_key=True)
    at: Mapped[datetime.datetime] = mapped_column("at", DateTime, default=functions.now())
    dV: Mapped[float] = mapped_column("dV", Double)  # unit is liter
    type: Mapped[str] = mapped_column("type", String(32))
    detail: Mapped[str] = mapped_column("detail", String(256))
    is_approx: Mapped[bool]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("model", choices=["Tank1Log", "Tank2Log", "Tank3Log", "Lock"])
    parser.add_argument("-a", "--action", choices=["delete", "adjust", "clear", "add", "show", "eval"], required=True)
    parser.add_argument("-t", "--target", )

    args = parser.parse_args()
    parser = configparser.ConfigParser()
    parser.read_file(open("alembic.ini"))

    engine = create_engine(parser["alembic"]["sqlalchemy.url"])
    Base.metadata.create_all(engine)
    with Session(engine) as session:
        model = globals()[args.model]
        if args.model.startswith("Tank"):
            if args.action == "clear":
                session.query(model).delete()
                session.commit()
            elif args.action == "eval":
                now_litre = session.query(func.coalesce(func.sum(Tank3Log.dV), 0)).scalar()
                print(f"now volume: {now_litre}L")
            elif args.action == "adjust":
                now_litre = session.query(func.coalesce(func.sum(Tank3Log.dV), 0)).scalar()
                dv = int(args.target) - now_litre
                instance = model(dV=dv, type="irrigation canal", detail="用水路からの取水による増加")
                if args.model == "Tank3Log":
                    instance.is_approx = True
                session.add(instance)


