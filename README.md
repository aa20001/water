# water_python
農業IoTでの水やり等

## Usage
### 水やり
水を1Lやる場合
```bash
watering.py -l 1.0
```

### タンクへのポンプ制御(継続的に動作させてください)
```bash
pump.py
```

### 気温、湿度、気圧の取得(BME280)
```bash
bme280.py
```

## Configuration
### pump.py
以下の設定を適時変更する
```
PUMP_PIN = 2  # pump pin number in bcm pinout
PUMP_SPEC = 240  # pump spec [L/h]
TANK3_MAXIMUM_LITRE = 600  # unit: [L]
TANK1_MAXIMUM_DISTANCE = 60  # unit: [mm]
TRIG_PIN = 4
ECHO_PIN = 27
```

### watering.py
以下の設定を適時変更する
```
PGM_ID = "watering"
PUMP_PIN = 27  # pump pin number in bcm pinout
PUMP_SPEC = 240  # pump spec [L/h]
```



